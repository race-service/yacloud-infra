module "compose-host" {
  source = "../../modules/cheap_compute_instance"

  name      = "compose-host"
  image_id  = data.yandex_compute_image.default.id
  subnet_id = data.yandex_vpc_subnet.default.id
  nat       = true
  ssh_keys  = "debian:${file("~/.ssh/id_rsa.pub")}"
}

locals {
  app_hosts = tolist([
    module.compose-host.external_ip
  ])
  inventory_yaml = (yamlencode({
    "app_hosts" : "${local.app_hosts}"
  }))
}

resource "local_file" "ansible_inventory" {
  filename        = "inventory.yml"
  content         = local.inventory_yaml
  file_permission = "0666"
}
