module "master-1" {
  source = "../../modules/cheap_compute_instance"

  name      = "kube-master-1"
  image_id  = data.yandex_compute_image.default.id
  subnet_id = data.yandex_vpc_subnet.default.id
  nat       = true
  ssh_keys  = "debian:${file("~/.ssh/id_rsa.pub")}"
}

module "worker-1" {
  source = "../../modules/cheap_compute_instance"

  name      = "kube-worker-1"
  image_id  = data.yandex_compute_image.default.id
  subnet_id = data.yandex_vpc_subnet.default.id
  nat       = true
  ssh_keys  = "debian:${file("~/.ssh/id_rsa.pub")}"
}

module "worker-2" {
  source = "../../modules/cheap_compute_instance"

  name      = "kube-worker-2"
  image_id  = data.yandex_compute_image.default.id
  subnet_id = data.yandex_vpc_subnet.default.id
  nat       = true
  ssh_keys  = "debian:${file("~/.ssh/id_rsa.pub")}"
}

locals {
  master_nodes   = tolist([
    module.master-1.external_ip
  ])
  worker_nodes   = tolist([
    module.worker-1.external_ip,
    module.worker-2.external_ip
  ])
  inventory_yaml = yamlencode({ 
    "master_nodes" : "${local.master_nodes}", 
    "worker_nodes" : "${local.worker_nodes}" 
  })
}

resource "local_file" "ansible_inventory" {
  filename = "inventory.yml"
  content  = local.inventory_yaml
}
