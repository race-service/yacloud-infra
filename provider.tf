terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "0.67.0"
    }
  }
}

provider "yandex" {
  service_account_key_file = "../../terraform-key.json"
  cloud_id                 = "b1gsg49kq3fou5n8aftj"
  folder_id                = "b1ghamjgkn33bujo6cik"
  zone                     = "ru-central-b"
}