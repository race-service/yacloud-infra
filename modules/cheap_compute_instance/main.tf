resource "yandex_compute_instance" "vm_instance" {
  name        = var.name
  platform_id = var.platform_id 

  resources {
    core_fraction = var.resources.core_fraction
    cores         = var.resources.cores
    memory        = var.resources.memory
  }

  boot_disk {
    initialize_params {
      image_id = var.image_id 
    }
  }

  network_interface {
    subnet_id = var.subnet_id 
    nat       = var.nat
  }

  metadata = {
    ssh-keys = var.ssh_keys
  }

  scheduling_policy {
    preemptible = var.preemptible
  }
}
