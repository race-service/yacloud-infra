output "instance_id" {
  description = "Created instance ID"
  value = yandex_compute_instance.vm_instance.id
}

output "external_ip" {
  description = "NAT IP-address of instance"
  value = yandex_compute_instance.vm_instance.network_interface.0.nat_ip_address
}

output "internal_ip" {
  description = "Internal IP-address of instance"
  value = yandex_compute_instance.vm_instance.network_interface.0.ip_address
}
