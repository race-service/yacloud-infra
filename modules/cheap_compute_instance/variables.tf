variable "name" {
  description = "Name of compute instance"
  type = string
}

variable "platform_id" {
  description = "See yandex_compute_instance.platform_id"
  type = string
  default = "standard-v3"
}

variable "resources" {
  description = "resource of vm. See yandex_compute_instance.resources"
  type = object({
    core_fraction = number
    cores = number
    memory = number
  })
  default = {
    core_fraction = 20
    cores = 2
    memory = 4
  }
}

variable "image_id" {
  description = "Id of image for boot disk"
  type = string
}

variable "subnet_id" {
  description = "Subnet id for VM"
  type = string
}

variable "nat" {
  description = ""
  type = bool
  default = true
}

variable "ssh_keys" {
  description = "ssh keys in format \"<user-name>:<ssh-key>\""
  type = string
}

variable "preemptible" {
  description = "set vm to preemptible(very cheaper)"
  type = bool
  default = true
}
