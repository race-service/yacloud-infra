# Race Service Yandex.Cloud deployment

## Requirements
- Terraform 0.13+
- Yandex.Cloud account
- Python 3(for backend generating)

## Getting started
1. Create Yandex.Cloud service account
2. Save IAM key for this account to filt `terraform-key.json`, e.g. by command `yc iam key create --service-account-name <my_account> -o terraform-key.json`
3. Copy file .yacloud.creds.yml.example to .yacloyd.creds.yml
4. In this file set `yandex.cloud_id` and `yandex.folder_id` to yours
5. If you want to save your state to Yandex.Cloud Object Storage
   1. You should create access key, e.g. with command `yc iam access-key create --service-account-name <my_account>`
   2. Set `backend.access_key` to access key ID, and `backend.secret_key` to secret key content
6. If you want to use other terraform backend, you should delete `backend` dict from `.yacloud.creds.yml` file
7. Run script `update_backend.py`
8. In folders you can find modules for service modules

## Create new deploy module
1. Create directory with naming `<environment>/<deploy_module_name>`, e.g. `dev/kubernetes`
2. In that directory run `ln -s {images,network,provider}.tf .
3. If you use my script for backends creation, walk to root repo directory and run `update_backend.py`
4. Write your module :)

## Use templates for custom backend
1. Change Jinja2 template `templates/backend.tf.j2` to what you want
2. Write your varibles, what you want use to `.yacloud.creds.yml` in `backend` section
3. Run `update_backend.py`
