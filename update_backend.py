#!/usr/bin/env python3
from jinja2 import Environment, FileSystemLoader, select_autoescape
import yaml
import os

CREDENTIALS_FILE='.yacloud.creds.yml'
TEMPLATES_FOLDER='templates'
BACKEND_TEMPLATE='backend.tf.j2'
PROVIDER_TEMPLATE='provider.tf.j2'
IGNORE_FOLDERS=['ansible', 'modules', 'templates']
BASE_PATH='.'

def scan_folder(path, formatter=None):
    with os.scandir(path) as it:
        for entry in it:
            if not entry.name.startswith('.') and not entry.name in IGNORE_FOLDERS and entry.is_dir():
                yield entry.name if formatter is None else formatter(entry.name)

def get_terraform_folders(base_path):
    for env_folder in scan_folder(base_path):
        path = os.path.join(base_path, env_folder)

        for module_name in scan_folder(path, lambda x: os.path.join(env_folder, x)):
            yield module_name


def rchop(s, suffix):
    if suffix and s.endswith(suffix):
        return s[:-len(suffix)]
    return s

def read_credentials():
    with open(CREDENTIALS_FILE, 'r') as f:
        try:
            return yaml.safe_load(f)
        except yaml.YAMLError as exc:
            print(exc)
            raise

def save_file(folder, tpl_name, content):
    path = os.path.join(folder, rchop(tpl_name, '.j2'))
    with open(path, 'w') as output:
        output.write(content)

env = Environment(
        loader=FileSystemLoader(TEMPLATES_FOLDER),
        autoescape=select_autoescape()
        )

backend_tpl=env.get_template(BACKEND_TEMPLATE)
provider_tpl=env.get_template(PROVIDER_TEMPLATE)


creds = read_credentials()

save_file(BASE_PATH, PROVIDER_TEMPLATE, provider_tpl.render(creds['yandex']))
if creds.get('backend') is not None:
    for module in get_terraform_folders(BASE_PATH):
        path = os.path.join(BASE_PATH, module)
        data = {**creds['backend'], 'state_path': module}
        save_file(path, BACKEND_TEMPLATE, backend_tpl.render(data))
